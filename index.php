<?php

require './vendor/autoload.php';

use FindPath\Finder;
use FindPath\Printer;
use Services\Csv\CsvReader;
use Services\DataDecorators\Beer;
use Services\DataDecorators\Brewery;
use Services\DataDecorators\Location;
use Services\Distance\HypotDistance;
use Services\Helpers;

const DISTANCE = 2000;

foreach ($argv as $argument) {
    if (strpos($argument, '--lat=') !== false) {
        $latitude = trim($argument, '--lat=');
    }

    if (strpos($argument, '--long=') !== false) {
        $longitude = trim($argument, '--long=');
    }
}

if (!$latitude || !$longitude) {
    throw new InvalidArgumentException('Please provide --lat and --long');
}

$geocodes = new CsvReader('./data/geocodes.csv');
$geocodes->decorate(new Location());
$locations = Helpers::readCsv($geocodes, 'id');

$beersCsv = new CsvReader('./data/beers.csv');
$beersCsv->decorate(new Beer());
$beers = Helpers::readCsv($beersCsv, 'id');;

$finder = new Finder(new HypotDistance(), $locations, $beers);
$finder->setLocation($latitude, $longitude);
$path = $finder->find(200);

$breweries = new CsvReader('./data/breweries.csv');
$breweries->decorate(new Brewery());
$breweries = Helpers::readCsv($breweries, 'id');

$home = new \FindPath\DataModels\Location(0, ['latitude' => $latitude, 'longitude' => $longitude]);

$printer = new Printer(new HypotDistance(), $locations, $beers, $breweries, $path, $home);
$printer->showResult();



