<?php

namespace Services\DataDecorators;

use FindPath\DataModels\Location as LocationModel;

class Location implements Decorator
{
    public function decorate(array $data)
    {
        if (!isset($data['brewery_id'])) {
            throw new \InvalidArgumentException('Value brewery_id missing in data array');
        }

        return new LocationModel($data['brewery_id'], $data);
    }
}