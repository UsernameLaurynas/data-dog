<?php

namespace Services\DataDecorators;

use FindPath\DataModels\Beer as BeerModel;

class Beer implements Decorator
{
    public function decorate(array $data)
    {
        return new BeerModel($data);
    }
}