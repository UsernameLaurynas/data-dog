<?php

namespace Services\DataDecorators;

use FindPath\DataModels\Brewery as BreweryModel;

class Brewery implements Decorator
{

    public function decorate(array $data)
    {
        if (!isset($data['id'])) {
            throw new \InvalidArgumentException('Value id missing in data array');
        }

        return new BreweryModel($data['id'], $data);
    }
}