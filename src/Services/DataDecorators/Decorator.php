<?php

namespace Services\DataDecorators;

interface Decorator
{
    public function decorate(array $data);
}