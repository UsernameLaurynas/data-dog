<?php

namespace Services\Csv;

use Services\DataDecorators\Decorator;

/**
 * Class CsvRead
 * @package Beer
 */
class CsvReader
{
    /**
     * @var string
     */
    private $fileDescriptor;

    /**
     * @var array
     */
    private $keys;

    /**
     * @var Decorator
     */
    private $decorator;

    /**
     * CsvRead constructor.
     * @param string $path
     */
    public function __construct($path)
    {
        $this->fileDescriptor = fopen($path, 'r');
        $this->keys = fgetcsv($this->fileDescriptor, 0, ",");
    }

    /**
     * @param Decorator $decorator
     */
    public function decorate(Decorator $decorator)
    {
        $this->decorator = $decorator;
    }

    /**
     * @return mixed
     */
    public function read()
    {
        $data = fgetcsv($this->fileDescriptor, 0, ",");
        if (!$data) {
            return null;
        }

        $data = array_combine($this->keys, $data);

        return $this->decorator ? $this->decorator->decorate($data) : $data;
    }
}