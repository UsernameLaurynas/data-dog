<?php

namespace Services;

use Services\Csv\CsvReader;

/**
 * Class Helpers
 * @package Services
 */
class Helpers
{
    /**
     * @param CsvReader $file
     * @param string $key
     */
    public static function readCsv(CsvReader $file, $key)
    {
        $result = [];
        while ($entry = $file->read()) {
            $result[$entry->$key] = $entry;
        }

        return $result;
    }
}