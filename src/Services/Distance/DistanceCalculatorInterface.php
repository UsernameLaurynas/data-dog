<?php

namespace Services\Distance;

use FindPath\DataModels\Location;

/**
 * Interface DistanceCalculatorInterface
 * @package Services\Distance
 */
interface DistanceCalculatorInterface
{
    /**
     * @param Location $a
     * @param Location $b
     * @return mixed
     */
    public function calculate(Location $a, Location $b);
}