<?php

namespace Services\Distance;

use FindPath\DataModels\Location;

class HypotDistance implements DistanceCalculatorInterface
{

    /**
     * @param Location $a
     * @param Location $b
     * @return mixed
     */
    public function calculate(Location $a, Location $b)
    {
        return hypot($b->latitude - $a->latitude, $b->longitude - $a->longitude);
    }
}