<?php

namespace FindPath;

use FindPath\DataModels\Location;
use Services\Distance\DistanceCalculatorInterface;

/**
 * Class Printer
 * @package FindPath
 */
class Printer
{
    /**
     * @var array
     */
    private $beers;
    /**
     * @var array
     */
    private $breweries;
    /**
     * @var DistanceCalculatorInterface
     */
    private $calculator;
    /**
     * @var array
     */
    private $locations;
    /**
     * @var array
     */
    private $path;
    /**
     * @var Location
     */
    private $home;

    /**
     * Printer constructor.
     * @param DistanceCalculatorInterface $calculator
     * @param array                       $locations
     * @param array                       $beers
     * @param array                       $breweries
     * @param array                       $path
     */
    public function __construct(
        DistanceCalculatorInterface $calculator,
        array $locations,
        array $beers,
        array $breweries,
        array $path,
        Location $home
    ) {

        $this->calculator = $calculator;
        $this->locations = $locations;
        $this->beers = $beers;
        $this->breweries = $breweries;
        $this->path = $path;
        $this->home = $home;
    }

    /**
     *
     */
    public function showResult()
    {
        $distance = 0;
        $previous = $this->home;

        $climate = new \League\CLImate\CLImate;
        $climate->info(sprintf('Found %d factories:', count($this->path) - 2));
        foreach ($this->path as $part) {
            if ($part === 0) {
                $distance += $this->calculator->calculate($previous, $this->home);
                $climate->tab(4)->info(
                    sprintf('-> HOME %f %f distance %dkm', $this->home->longitude, $this->home->latitude, $distance)
                );
                continue;
            }

            $distance += $this->calculator->calculate($previous, $this->locations[$part]);
            $climate->tab(4)->info(
                sprintf(
                    '-> [%d] %s: %f %f distance %dkm',
                    $part,
                    $this->breweries[$part]->name,
                    $this->locations[$part]->longitude,
                    $this->locations[$part]->latitude,
                    $distance
                )
            );
        }
    }
}