<?php

namespace FindPath\DataModels;

/**
 * @property int id
 * @property float longitude
 * @property float latitude
 */
/**
 * Class Location
 * @package FindPath\DataModels
 */
class Location extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var float
     */
    protected $longitude;
    /**
     * @var float
     */
    protected $latitude;

    /**
     * Beer constructor.
     */
    public function __construct($id, array $data)
    {
        $this->id = $id;
        $this->longitude = $this->pluckValue($data, 'longitude');
        $this->latitude = $this->pluckValue($data, 'latitude');
    }
}