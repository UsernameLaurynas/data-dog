<?php

namespace FindPath\DataModels;

/**
 * Class Brewery
 * @package FindPath\DataModels
 */
class Brewery extends AbstractModel
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;

    /**
     * Beer constructor.
     */
    public function __construct($id, array $data)
    {
        $this->id = $id;
        $this->longitude = $this->pluckValue($data, 'name');
    }
}