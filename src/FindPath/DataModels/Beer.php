<?php

namespace FindPath\DataModels;

/**
 * @property int id
 * @property int breweryId
 */
/**
 * Class Beer
 * @package FindPath\DataModels
 */
class Beer extends AbstractModel
{
    /**
     * @var int
     */
    protected $breweryId;
    /**
     * @var int
     */
    protected $id;

    /**
     * Beer constructor.
     */
    public function __construct(array $data)
    {
        $this->id = $this->pluckValue($data, 'id');
        $this->breweryId = $this->pluckValue($data, 'brewery_id');
    }
}