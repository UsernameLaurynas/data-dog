<?php

namespace FindPath\DataModels;

/**
 * Class AbstractModel
 * @package FindPath\DataModels
 */
abstract class AbstractModel
{
    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (!property_exists(static::class, $name)) {
            throw new \InvalidArgumentException(sprintf('Property %s does not exists in %s class', $name, self::class));
        }

        return $this->$name;
    }

    /**
     * @param array $data
     * @param       $name
     * @return mixed
     */
    protected function pluckValue(array $data, $name)
    {
        if (!isset($data[$name])){
            throw new \InvalidArgumentException(sprintf('Value %s missing in data array'));
        }

        return $data[$name];
    }
}