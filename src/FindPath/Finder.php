<?php

namespace FindPath;

use FindPath\DataModels\Beer;
use FindPath\DataModels\Location;
use Services\Distance\DistanceCalculatorInterface;

/**
 * Class Finder
 * @package FindPath
 */
class Finder
{
    /**
     * @var Beer[]
     */
    private $beers;
    /**
     * @var DistanceCalculatorInterface
     */
    private $calculator;
    /**
     * @var Location[]
     */
    private $locations;
    /**
     * @var Location
     */
    private $home;

    /**
     * Finder constructor.
     * @param DistanceCalculatorInterface $calculator
     * @param array                       $locations
     * @param array                       $beers
     */
    public function __construct(DistanceCalculatorInterface $calculator, array $locations, array $beers)
    {
        $this->locations = $locations;
        $this->beers = $beers;
        $this->calculator = $calculator;
    }

    /**
     * @param float $latitude
     * @param float $longitude
     */
    public function setLocation($latitude, $longitude)
    {
        if (!is_numeric($latitude) || !is_numeric($longitude)) {
            throw new \InvalidArgumentException('Latitude and Longitude must be numeric');
        }

        $this->home = new Location(0, ['latitude' => (float) $latitude, 'longitude' => (float) $longitude]);
    }

    /**
     * @return int[]
     */
    public function find($pathDistance)
    {
        if (!$this->home) {
            throw new \BadMethodCallException('Before calling find, call setLocation');
        }

        $distances = $this->getDistances();
        $beersInBreweries = $this->getBeersCounts();
        $distancesPerBottle = $this->calculateDistancesPerBottle($distances, $beersInBreweries);

        $remainingDistance = $pathDistance;
        $path = [];
        $currentLocation = 0;

        while ($remainingDistance > 0) {
            $exclude = array_merge($path, [$currentLocation]);
            $brewery = $this->findClosest($distancesPerBottle[$currentLocation], $exclude);
            if ($remainingDistance - $distances[$currentLocation][$brewery] - $distances[$currentLocation][0] < 0) {
                $brewery = $this->findClosest($distances[$currentLocation], $exclude);
            }
            if ($remainingDistance - $distances[$currentLocation][$brewery] - $distances[$currentLocation][0] < 0) {
                $path[] = $currentLocation;
                break;
            }

            $remainingDistance -= $distances[$currentLocation][$brewery];
            $path[] = $currentLocation;
            $currentLocation = $brewery;
        }

        $path[] = 0;

        return $path;
    }

    /**
     * @param $distances
     * @param $beersInBreweries
     * @return float[][]
     */
    private function calculateDistancesPerBottle($distances, $beersInBreweries)
    {
        $result = [];
        foreach ($beersInBreweries as $brewery => $count) {
            foreach ($distances as $breweryFrom => $distancesTo) {
                if (!isset($distancesTo[$brewery])) {
                    continue;
                }
                $result[$breweryFrom][$brewery] = $distancesTo[$brewery] / $count;
            }
        }

        return $result;
    }

    /**
     * @param float[] $distances
     * @return int
     */
    private function findClosest($distances, $exclude = [])
    {
        $minDistance = null;
        $minDistanceLocation = null;
        foreach ($distances as $destination => $distance) {
            if (in_array($destination, $exclude)) {
                continue;
            }
            if (null !== $minDistance && $minDistance < $distance) {
                continue;
            }

            $minDistanceLocation = $destination;
            $minDistance = $distance;
        }

        return $minDistanceLocation;
    }

    /**
     * @return int[]
     */
    private function getBeersCounts()
    {
        $result = [];
        foreach ($this->beers as $beer) {
            if (!isset($result[$beer->breweryId])) {
                $result[$beer->breweryId] = 0;
            }
            $result[$beer->breweryId]++;
        }

        return $result;
    }

    /**
     * @return float[][]
     */
    private function getDistances()
    {
        $distances = [];
        $locations = array_merge([$this->home], $this->locations);

        foreach ($locations as $from) {
            foreach ($locations as $to) {
                $distances[$from->id][$to->id] = $this->calculator->calculate($from, $to);
            }
        }

        return $distances;
    }
}